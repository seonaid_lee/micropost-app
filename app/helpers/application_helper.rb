module ApplicationHelper
	# Returns the full title on a per-page basis (with no leader if no title is provided)
	def full_title(page_title = '')
		base_title = "Cosmic Conversations"
		if page_title.empty?
			base_title
		else
			page_title + ' | ' + base_title
		end
	end

end
